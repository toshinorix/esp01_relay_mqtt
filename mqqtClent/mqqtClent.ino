#include <PubSubClient.h>
#include <ESP8266WiFi.h>

const char* ssid = "oooo";
const char* password = "pmgana921";
const char* mqttServer = "192.168.1.5";
const int mqttPort = 1883;
const char* mqttUser = "admin";
const char* mqttPassword = "pmgana921";
const char* clientName =    "ESP03";
const char* topic_avail =   "/home/ESP03/available";
const char* topic_state =   "/home/ESP03/relay/state";
const char* topic =         "/home/ESP03/relay/gpio/0";
String avail = "online";
const int buttonPin = 2;
int blinkTime = 500;
int RELAY = 0;                            // GPIO pin of relay

int pin = 2;
volatile int state = false;
volatile int flag = false;
const char* door_state = "closed";

unsigned long previousMillis = 0; 
const long interval = 3000;

void changeDoorStatus() {

    unsigned long currentMillis = millis();
 
    if(currentMillis - previousMillis >= interval) {
        previousMillis = currentMillis;   
    
        state = !state;
        if(state) {
            door_state = "opened";
        }
        else{
            door_state = "closed";
        }
        flag = true;
        Serial.println(state);
        Serial.println(door_state);
    }
    
}


WiFiClient wifiClient3;
PubSubClient client(wifiClient3);


void setup() {

  
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(RELAY, OUTPUT);
  digitalWrite(RELAY, LOW);
  digitalWrite(LED_BUILTIN, LOW);
  Serial.begin(115200);
  Serial.println(clientName);
  delay(20);

  initWiFi();
 

  initMQTT();


  Serial.println("Preparing the Door Status Monitor project...");
    
    pinMode(pin, OUTPUT);
    attachInterrupt(digitalPinToInterrupt(pin), changeDoorStatus, CHANGE);


}

void loop() {

   

 const unsigned long minu = 1 * 60 * 1000UL;
 static unsigned long lSTime = 0 - minu;
 unsigned long n = millis();
 if (n - lSTime >= minu)
 {
    lSTime += minu;
           if(!client.connected()){
            blinkY(3, 50);
            initMQTT();
            }
            else{
                  client.publish(topic_avail, (char*) avail.c_str());
              }
 }



  
  //  // Publikacja statusu dostepności
  //  static int counter = 0;
  //  String avail = "1";
  //  if (client.connected()) {
  //    Serial.print("Sending payload: ");
  //    Serial.println(avail);
  //
  //    if (client.publish(topic_avail, (char*) avail.c_str())) {
  //      Serial.println("Publish ok");
  //    }
  //    else {
  //      Serial.println("Publish failed");
  //    }
  //  }
  //  ++counter;
  //  delay(15000);
  //  // Publikacja statusu dostepności
//  BUTTONState = digitalRead(BUTTON);
//  if (BUTTONState == HIGH)
//  {
//    digitalWrite(LED_BUILTIN, HIGH);
//  }
//  else
//  {
//    digitalWrite(LED_BUILTIN, LOW);
//  }

 
}



void initWiFi() {
  Serial.println("Connecting to ");
  Serial.print(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  };  Serial.println("Connected to AP");
}
void initMQTT(){
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);

  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");

    if (client.connect(clientName, mqttUser, mqttPassword )) {

      Serial.println("connected ");
      Serial.println("availability ... ");
      client.publish(topic_avail, (char*) avail.c_str());


      blinkY(3, 500);

    } else {

      Serial.print("failed with state ");
      Serial.print(client.state());
      blinkY(3, 50);

      

    }
  }
     
  client.subscribe(topic);
  
  
  }
//void publishState(char* state) {
//
//  Serial.print("publish to state :");
//  Serial.print(state);
//  client.publish(topic_state, "1");
//};

void callback(char* topicc, byte* payload, unsigned int length) {
  
  Serial.println(topicc);
  for (int i = 0; i < length; i++) {
    if ((char)payload[0] == '1') {
      Serial.print("Relay turning OFF");
      digitalWrite(RELAY, HIGH);
      client.publish(topic_state, "1");
      digitalWrite(LED_BUILTIN, HIGH);
    }
    if ((char)payload[0] == '0') {
      Serial.print("Relay turning ON");
      digitalWrite(RELAY, LOW);
      client.publish(topic_state, "0");
      digitalWrite(LED_BUILTIN, LOW);
    }
  }
  //    ESP.deepSleep(0);
}




void blinkY(int repeats, int time)
{
  for (int i = 0; i < repeats; i++)
  {
    digitalWrite(LED_BUILTIN, LOW);
    delay(time);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(time);
  }
}
